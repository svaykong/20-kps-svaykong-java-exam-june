package com.example.demo.service;
import com.example.demo.repository.DTO.Article;
import java.util.List;

public interface ArticleService {

    Article insert(Article article);
    List<Article> getData();
    Article update(Article article);
    boolean delete(String code);
    Article findCode(String code);

}
