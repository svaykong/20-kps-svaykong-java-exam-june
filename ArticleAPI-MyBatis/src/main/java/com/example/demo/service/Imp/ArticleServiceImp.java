package com.example.demo.service.Imp;

import com.example.demo.repository.ArticleRepository;
import com.example.demo.repository.DTO.Article;
import com.example.demo.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImp implements ArticleService {

    private ArticleRepository articleRepository;
    @Autowired
    public void setArticleRepository(ArticleRepository articleRepository){
        this.articleRepository=articleRepository;
    }
    @Override
    public Article insert(Article article) {
        boolean isInserted =articleRepository.insert(article);
        if(isInserted)
            return article;
        else
            return null;
    }

    @Override
    public List<Article> getData() {
        return articleRepository.getData();
    }

    @Override
    public Article update(Article article) {
        boolean isUpdated =articleRepository.update(article);
        if(isUpdated)
            return article;
        else
            return null;
    }

    @Override
    public boolean delete(String code) {
        return articleRepository.delete(code);
    }

    @Override
    public Article findCode(String code) {
        return articleRepository.findCode(code);
    }


}
