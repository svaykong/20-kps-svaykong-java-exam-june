package com.example.demo.restController;

import com.example.demo.repository.DTO.Article;
import com.example.demo.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/articles")
public class ArticleRestController {

    private ArticleService articleService;
    @Autowired
    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }
    @RequestMapping(method=RequestMethod.GET)
    public Map<String,Object> getArticle(){
        Map<String,Object> result=new HashMap<>();
        List<Article> articles=articleService.getData();
        if(articles.isEmpty()){
            result.put("Message","No data in database");
        }else{
            result.put("Message","get seccessfully");
        }
        result.put("DATA",articles);
        return result;
    }
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String,Object> addArticle(@RequestBody Article article){
        Map<String,Object> result=new HashMap<>();
        articleService.insert(article);
        result.put("Message","Post seccessfully");
        result.put("DATA",article);
        return result;
    }
    @RequestMapping(value = "{code}",method=RequestMethod.PUT)
    public Map<String,Object> updateArticle(@RequestParam String code,@RequestBody Article article){
        Map<String,Object> result=new HashMap<>();
        Article article1=articleService.findCode(code);
        if(article1==null){
            result.put("Message","CODE Not Found Update Unsuccessfully");
            result.put("Response Code",404);
        }else{
            articleService.update(article);
            result.put("Message","Update Successfully");
            result.put("Response Code",200);
            result.put("DATA",article);
        }
        return result;
    }
    @RequestMapping(value = "{code}",method=RequestMethod.DELETE)
    public Map<String,Object> deleteArticle(@RequestParam String code){
        Map<String,Object> result=new HashMap<>();
        Article article1=articleService.findCode(code);
        if(article1==null){
            result.put("Message","CODE Not Found Delete Unsuccessfully");
            result.put("Response Code",404);
        }else{
            articleService.delete(code);
            result.put("Message","Delete Successfully");
            result.put("Response Code",200);
        }
        return result;
    }




}

