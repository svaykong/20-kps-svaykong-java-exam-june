package com.example.demo.configuration;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
@Configuration
public class jdbcConfiguration {
    @Bean
    public DataSource myPostgresDb(){
        DataSourceBuilder dataSource = DataSourceBuilder.create();
        dataSource.driverClassName("org.postgresql.Driver");
        dataSource.url("jdbc:postgresql://localhost:5432/article_db");
        dataSource.username("postgres");
        dataSource.password("postgres");
        return dataSource.build();
    }
}

